import taichi as ti
from PIL import Image
import numpy as np
from numpy import asarray

ti.init(arch=ti.cuda)

# here we define an invert function on val
@ti.kernel
def do_invert():
    for i in range(dims):
        val[i] = 255 - val[i]
        

image = Image.open('dog.jpg')

# Converting to a Numpy array
data = asarray(image, dtype=np.uint8)
print('Image shape: ', data.shape, data.dtype)

# Retrieving image dimensions
n = data.shape[0]
m =  data.shape[1]
dims = n * m * 3

# Creating the Taichi array
val = ti.var(ti.u8, shape=(dims))
arr = data.ravel()
val.from_numpy(arr)

# Calling function
do_invert()

# Converting back to numpy
arr = val.to_numpy()
# Back to normal dimensions
arr = np.reshape(arr, (n, m, 3))

result = Image.fromarray(arr)
result.save('dog_i.jpg')
