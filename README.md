# HPC Project 2: Analyze an open source tool

> Tiago Povoa Quinteiro - HEIG-VD - Course: High Performance Coding

Assignement:

> Details: We will propose a list of tools, libraries, languages, and more. You have to choose one of them; you have to install it on your machine, run it and develop an example of use not directly derived from the official documentation of the tool.

## Introduction

For my assignment, I have to look into Taichi. It is a domain-specific programming language designed for highly parallel tasks in CPU and GPU. You can integrate it easily in a python project. 

### Installation

You'll need python 3.6/3.7/3.8

Using pip:

`python3 -m pip install taichi`

More details: https://taichi.readthedocs.io/en/latest/install.html

### Hello world example

You can test it with this example

https://taichi.readthedocs.io/en/latest/hello.html

## How to use it

First, you have to import: `import taichi as ti`

Then, specify the wanted architecture `ti.init(arch=ti.gpu)` (gpu / cuda / opengl / metal). There is some differences in support. You might also need to specify memory on some architectures. For this, I'll leave you with the official documentation.

Now, you can add both `@ti.kernel` or `@ti.func` annotations above a function and write your taichi code inside. 

`@ti.func` are functions you can call in a taichi scope.

## Example

To keep this example as simple as possible, I chose to do a basic invert on an image.

I used Numpy and Pillow dependencies since they are familiar to the Python world, but you could pick different solutions of course.

You can see in my `example.py` that I start by reading the image file, then converting it from a 3D array to a more convenient 1D format. Then I prepare a Taichi array variable and call my function.

```python
val = ti.var(ti.u8, shape=(dims))
```

As we can see, we have to specify the data type as `ti.u8`, equivalent to uint8_t.

> Note: it seems that some types are not supported in all plateforms. more info here: https://taichi.readthedocs.io/en/latest/type.html

In the example bellow, we can now iterate over the data. And again, we are working on parallel.  

```python
@ti.kernel
def do_invert():
    for i in range(dims): # This for loop should in theory be parallelized
        val[i] = 255 - val[i]
```

In this very simple example, we don't need the contiguous pixels to calculate anything. However, if you needed to access it, you might run into concurrency problems.  For more information see: https://taichi.readthedocs.io/en/latest/atomic.html

Also, for more complex calculation where you'd need to multiply pixels with matrices see: https://taichi.readthedocs.io/en/latest/tensor_matrix.html#tensors-of-matrices

## Sources

https://taichi.readthedocs.io/en/latest/index.html

https://github.com/taichi-dev/taichi

https://www.youtube.com/watch?v=wKw8LMF3Djo

## Conclusion

One of the most surprising aspects about Taichi is how easy it is to integrate it. It's simply a python package. It could be a very promising, however not easy task, to see it more used in machine learning algorithms as it's one of the current trends of the python language. 

My example was a bit simplistic, but I hope it provided some beginner friendly informations. 